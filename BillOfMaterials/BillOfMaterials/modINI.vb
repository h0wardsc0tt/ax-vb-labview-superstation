﻿Module modINI

    'API DECLARATIONS
    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Function WritePrivateProfileString Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal Section As String, ByVal Key As String, ByVal putStr As String, ByVal INIfile As String) As Integer

    Public Function ReadINI(ByVal sSection As String, ByVal sKey As String, ByVal sIniFileName As String)

        Dim nLength As Integer
        Dim sTemp As String
        Dim lsTemp As Integer

        sTemp = Space(255)
        lsTemp = Len(sTemp)
        nLength = GetPrivateProfileString(sSection, sKey, "", sTemp, lsTemp, sIniFileName)

        Return Left(sTemp, nLength)

    End Function

    Public Sub WriteINI(ByVal sINIFile As String, ByVal sSection As String, ByVal sKey As String, ByVal sValue As String)

        Dim n As Integer
        Dim sTemp As String

        sTemp = sValue

        'Replace any CR/LF characters with spaces
        For n = 1 To Len(sValue)
            If Mid$(sValue, n, 1) = vbCr Or Mid$(sValue, n, 1) = vbLf _
            Then Mid$(sValue, n) = " "
        Next n
        WritePrivateProfileString(sSection, sKey, sTemp, sINIFile)

    End Sub

End Module