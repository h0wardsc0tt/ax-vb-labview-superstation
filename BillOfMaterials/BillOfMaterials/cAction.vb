﻿
Option Explicit On

Imports System.IO
Imports IBMU2.UODOTNET

Imports System.Reflection ' For Missing.Value and BindingFlags
Imports System.Runtime.InteropServices  ' For COMException
Imports Microsoft.Office.Interop.Excel
Imports System.Data.SqlClient

Public Class cAction

    Dim uonetSession As IBMU2.UODOTNET.UniSession
    Dim uonetCMD As IBMU2.UODOTNET.UniCommand
    Dim uonetSubroutine As IBMU2.UODOTNET.UniSubroutine
    Dim uonetFile As IBMU2.UODOTNET.UniFile
    Dim uonetDataset As IBMU2.UODOTNET.UniDataSet
    Dim uonetArray As IBMU2.UODOTNET.UniDynArray
    Dim strResponse As String
    Dim msg As String

    Dim txtHostname As String = "Avante"
    Dim txtDatabase As String = "LIVE.DATA"
    Dim txtUID As String = "superstation"
    Dim txtPWD As String = "N9TS29VP7BE84PZ1DP3AR5YU17SW4WR4"
    Dim txtProcess As String = "udcs"
    Dim txtAvanteSubroutine As String = "BOMS9656.2"
    Dim txtBOMDir As String = "C:\BOM"
    Dim txtBOMFile As String = "BOMData1.xls"

    'Excel object
    Dim objExcelApp As New Microsoft.Office.Interop.Excel.Application

    Dim U2Array As UniDynArray
    Dim U2Array1 As UniDynArray  'Heading 1
    Dim U2Array2 As UniDynArray  'Heading 2
    Dim U2Array3 As UniDynArray  'Heading 3
    Dim U2Array4 As UniDynArray  'Heading 4
    Dim U2Array5 As UniDynArray  'Heading 5
    Dim U2Array6 As UniDynArray  'Heading 6
    Dim U2Array7 As UniDynArray  'Column headings
    Dim U2Array8 As UniDynArray  'Part Data
    Dim sArray(15) As String 'DL ARRAY

    Dim dataParts As String 'Data returned
    Dim PartNoCnt As Integer  'Number of lines of parts
    Dim workbooks As Workbooks
    Dim workbook As Workbook
    Dim sheets As Sheets
    Dim worksheet1 As _Worksheet
    Dim endNumPart As Integer
    Dim rangePart As Object
    Dim endStrQty As String
    Dim rangeCompDt As Object

    Dim HeadingCnt1 As Integer  ' Number heading 1 entries
    Dim HeadingCnt2 As Integer  ' Number heading 2 entries
    Dim HeadingCnt3 As Integer  ' Number heading 3 entries
    Dim HeadingCnt4 As Integer  ' Number heading 4 entries
    Dim HeadingCnt5 As Integer  ' Number heading 5 entries
    Dim HeadingCnt6 As Integer  ' Number heading 6 entries
    Dim ColHeadingCnt As Integer  ' Number column heading entries
    Dim linesCnt As Integer     ' Number of lines of data
    Dim infoCnt As Integer      ' Number of colums in line of data

    Dim sPartNumber As String = String.Empty
    Dim sRevision As String = String.Empty

    Private Sub ConnectUniData()

        Try
            uonetSession = UniObjects.OpenSession(txtHostname, txtUID, txtPWD, txtDatabase, txtProcess)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub 'ConnectUniData()

    Private Sub DisConnectUniData()

        If uonetSession Is Nothing Then
        Else
            If uonetSession.IsActive Then
                Try
                    UniObjects.CloseSession(uonetSession)
                    uonetSession = Nothing
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
            End If
        End If
    End Sub 'DisConnectUniData()

    Public Sub DownloadBOM(ByVal PartNumber As String, ByVal RevLevel As String)
        ''System.Diagnostics.Debugger.Launch()

        'Remove spaces and the null char

        PartNumber = PartNumber.Trim(" "c, ControlChars.NullChar, ControlChars.NewLine)
        RevLevel = RevLevel.Trim(" "c, ControlChars.NullChar, ControlChars.NewLine)

        sPartNumber = PartNumber.ToString()
        sRevision = RevLevel.ToString()

        Dim ds As New DataSet()

        Call GetBom(sPartNumber, ds)

        Dim newline As Char = "ý"
        Dim newcol As Char = "ü"
        Dim strDetail As String = String.Empty

        For Each row As DataRow In ds.Tables(2).Rows
            If strDetail.Length = 0 Then
                strDetail = (Convert.ToInt32(row("LineNum"))).ToString() & newcol & row("ItemId") & newcol & row("ECMRevLevelDrawing") & newcol & row("AM_RoHSCompliant") & newcol & newcol & row("NameAlias") & newcol & row("UnitId") & newcol & If(row("BomQty").ToString() = "", "", (Convert.ToInt32(row("BomQty"))).ToString()) & newcol & newcol & newcol & row("RefDesignator") & newcol & If(row("PhysicalInvent").ToString() = "", "", (Convert.ToInt32(row("PhysicalInvent"))).ToString()) & newcol & row("ManPartNo") & newcol
            Else
                strDetail += newline & (Convert.ToInt32(row("LineNum"))).ToString() & newcol & row("ItemId") & newcol & row("ECMRevLevelDrawing") & newcol & row("AM_RoHSCompliant") & newcol & newcol & row("NameAlias") & newcol & row("UnitId") & newcol & If(row("BomQty").ToString() = "", "", (Convert.ToInt32(row("BomQty"))).ToString()) & newcol & newcol & newcol & row("RefDesignator") & newcol & If(row("PhysicalInvent").ToString() = "", "", (Convert.ToInt32(row("PhysicalInvent"))).ToString()) & newcol & row("ManPartNo") & newcol

            End If
        Next row

        'MsgBox("Start of DownloadBOM")

        If Len(txtAvanteSubroutine) = 0 Then
            MsgBox("Subroutine name field Is blank ! Error 6")
            Exit Sub
        End If

        Try
            'Connect to UniData - login
            ConnectUniData()
            'MsgBox("Connected to UniData")
        Catch ex As Exception
            MsgBox(ex.Message + "  Error 01")
            Exit Sub
        End Try


        If uonetSession.IsActive Then
            Try
                Try
                    ' Avante subroutine name and it has 3 arguments.
                    uonetSubroutine = uonetSession.CreateUniSubroutine(txtAvanteSubroutine, 3)
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 1")
                    Exit Sub
                End Try


                'Returned BOM data
                Try
                    uonetSubroutine.SetArg(0, "")
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 2A")
                    Exit Sub
                End Try
                'Part Number
                Try
                    uonetSubroutine.SetArg(1, PartNumber)
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 2B")
                    Exit Sub
                End Try
                'Revision level
                Try
                    uonetSubroutine.SetArg(2, RevLevel)
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 2C")
                    Exit Sub
                End Try

                'Call the subroutine
                Try
                    uonetSubroutine.Call()
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 3")
                    Exit Sub
                End Try

                'Data returned from subroutine in Avante - No errors encountered

                'dataParts = uonetSubroutine.GetArg(0)

                'U2Array = uonetSession.CreateUniDynArray(dataParts)

                Dim from_Date As Date
                Dim fromDate As String = String.Empty
                If (Not ds.Tables(1)(0)("FromDate") Is Nothing) Then
                    If Date.TryParse(ds.Tables(1)(0)("FromDate").ToString(), from_Date) Then
                        fromDate = from_Date.ToString("MM/dd/yyyy")
                    End If
                End If
                Dim ItemId As String = ds.Tables(1)(0)("ItemId").ToString()
                Dim UnitId As String = ds.Tables(1)(0)("UnitId").ToString()
                Dim parent As String = "Parent: ý" & ds.Tables(1)(0)("ItemId").ToString() & "ýýUOM: ý" & ds.Tables(1)(0)("UnitId").ToString() & "ý"
                Dim U2Array_1 As UniDynArray = uonetSession.CreateUniDynArray("BOMS9656.2 - Master Item List with References (Exploded)")
                Dim U2Array_2 As UniDynArray = uonetSession.CreateUniDynArray(parent)
                Dim U2Array_3 As UniDynArray = uonetSession.CreateUniDynArray("ý" & ds.Tables(1)(0)("NameAlias").ToString() & "ýýRequested Rev: ý" & ds.Tables(1)(0)("ECMRevLevelDrawing").ToString() & "ýMfg Rev: ýýEng Rev: ýý")
                Dim U2Array_4 As UniDynArray = uonetSession.CreateUniDynArray("ýýýEff Date: ý" & fromDate & "ý")
                Dim U2Array_5 As UniDynArray = uonetSession.CreateUniDynArray("ýýýRGB_LINE: ýý")
                Dim U2Array_6 As UniDynArray = uonetSession.CreateUniDynArray("DOWNLOAD DATE: ýý02/09/17ý20:49:51ý")
                Dim U2Array_7 As UniDynArray = uonetSession.CreateUniDynArray("SeqýItem NbrýRevýRoHSýNSýDescriptionýUOMýQTYýComp TypeýComp TypeýReference DesignatorsýQOHýMfg Part NbrýRGB Lineý")
                Dim U2Array_8 As UniDynArray = uonetSession.CreateUniDynArray(strDetail)

                'U2Array3 = U2Array.Extract(3)
                'U2Array3 = U2Array.Extract(4)
                U2Array1 = U2Array_1 'U2Array.Extract(1) 'Heading 1
                U2Array2 = U2Array_2 'U2Array.Extract(2) 'Heading 2
                U2Array3 = U2Array_3 'U2Array.Extract(3) 'Heading 3
                U2Array4 = U2Array_4 'U2Array.Extract(4) 'Heading 4
                U2Array5 = U2Array_5 'U2Array.Extract(5) 'Heading 5
                U2Array6 = U2Array_6 'U2Array.Extract(6) 'Heading 6
                U2Array7 = U2Array_7 'U2Array.Extract(7) 'Column Headings
                U2Array8 = U2Array_8 'U2Array.Extract(8) 'Data
                'MsgBox("Arrays created")

                HeadingCnt1 = U2Array1.Dcount(1) ' Number heading 1 entries
                'MsgBox("Number HeadingCnt1 = " + HeadingCnt1.ToString)
                HeadingCnt2 = U2Array2.Dcount(1) ' Number heading 2 entries
                'MsgBox("Number HeadingCnt2 = " + HeadingCnt2.ToString)
                HeadingCnt3 = U2Array3.Dcount(1) ' Number heading 3 entries
                HeadingCnt4 = U2Array4.Dcount(1) ' Number heading 4 entries
                HeadingCnt5 = U2Array5.Dcount(1) ' Number heading 5 entries
                HeadingCnt6 = U2Array6.Dcount(1) ' Number heading 6 entries
                ColHeadingCnt = U2Array7.Dcount(1) ' Number column heading entries
                'MsgBox("Number ColHeadingCnt = " + ColHeadingCnt.ToString)


                ' Excel spreadsheet
                Try
                    objExcelApp.Visible() = False
                    'MsgBox("Spreadsheet set to Not be visible")
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 10")
                    Exit Sub
                End Try

                Try
                    workbooks = objExcelApp.Workbooks
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 11")
                    Exit Sub
                End Try

                Try
                    workbook = workbooks.Add(XlWBATemplate.xlWBATWorksheet)
                    'MsgBox("workbook created")
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 13")
                    Exit Sub
                End Try


                Try
                    sheets = workbook.Worksheets
                    'MsgBox("sheets created")
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 15")
                    Exit Sub
                End Try

                Try
                    worksheet1 = sheets.Item(1)
                    If worksheet1 Is Nothing Then
                        MsgBox("Error: Worksheet == null" + "Error 4")
                        Exit Sub
                    End If
                    'MsgBox("worksheet1 created")
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 16")
                    Exit Sub
                End Try

                'Sheet tab name is "Data"
                worksheet1.Name = "Data"
                'Sheet title is not used
                objExcelApp.Caption = "Bill of Materials"

                'MsgBox("Now run Setup_Spreadsheet")

                Setup_Spreadsheet() 'Set up the spreadsheet from the data returned

            Catch ex As Exception
                MsgBox(ex.Message + " Error 17")
                Exit Sub
            End Try


            ExitBom() 'Exit the spreadsheet and quit

        End If
        '
    End Sub 'DownloadBOM()


    Private Sub ExitBom()

        If uonetSession Is Nothing Then

            'Destroy the Excel object reference
            objExcelApp = Nothing

        Else
            'If uonetSession.IsActive Then
            Try
                Try
                    DisConnectUniData() ' Disconnect from Avanté
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 20")
                    Exit Sub
                End Try


                'Save and Quit Excel
                Try
                    WorksheetSaveAs() 'New file
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 21")
                    Exit Sub
                End Try

                Try
                    objExcelApp.Quit()
                Catch ex As Exception
                    MsgBox(ex.Message + "  Error 22")
                    Exit Sub
                End Try


                'Destroy the Excel object reference
                objExcelApp = Nothing

            Catch Outer As COMException
                MsgBox("User closed Excel manually, so we don't have to do that - Error 5")
            End Try

            'End If
        End If
        '
    End Sub 'ExitBom()

    Private Sub Setup_Spreadsheet()

        worksheet1.Columns.Clear() 'Clear worksheet

        ' CALL DOCUMENT LOCATOR TO GET DOWN FILES AND CREATE AN ARRAY FOR ADDING TO THE SPREADSHEET
        Call AddDocumentLocatorFiles()

        'Heading 1
        Dim rangeHeading1 As Object
        Dim endRangeHeading1 As String
        Dim endCellNumber As Integer
        endCellNumber = HeadingCnt1 + 16
        endRangeHeading1 = ChrW(65 + endCellNumber) + "1"
        rangeHeading1 = worksheet1.Range("A1", endRangeHeading1)
        Dim heading1Array(HeadingCnt1 + 16) As String
        Dim ik As Integer
        For ik = 0 To HeadingCnt1 - 1
            heading1Array(ik) = U2Array1.Extract(1, ik + 1).ToString
        Next

        ' ADD DOCUMENT LOCATOR'S 8 ROWS
        For ik = 1 To 16
            heading1Array(ik) = sArray(ik - 1)
        Next

        rangeHeading1.Value2 = heading1Array
        'rangeHeading1.HorizontalAlignment = -4108 'Center in cells

        'Heading 2
        Dim rangeHeading2 As Object
        Dim endRangeHeading2 As String
        endCellNumber = HeadingCnt2
        endRangeHeading2 = ChrW(65 + endCellNumber) + "2"
        rangeHeading2 = worksheet1.Range("A2", endRangeHeading2)
        Dim heading2Array(HeadingCnt2) As String
        For ik = 0 To HeadingCnt2 - 1
            heading2Array(ik) = U2Array2.Extract(1, ik + 1).ToString
        Next
        rangeHeading2.Value2 = heading2Array
        'rangeHeading2.HorizontalAlignment = -4108 'Center in cells

        'Heading 3
        Dim rangeHeading3 As Object
        Dim endRangeHeading3 As String
        endCellNumber = HeadingCnt3
        endRangeHeading3 = ChrW(65 + endCellNumber) + "3"
        rangeHeading3 = worksheet1.Range("A3", endRangeHeading3)
        Dim heading3Array(HeadingCnt3) As String
        For ik = 0 To HeadingCnt3 - 1
            heading3Array(ik) = U2Array3.Extract(1, ik + 1).ToString
        Next
        rangeHeading3.Value2 = heading3Array
        'rangeHeading3.HorizontalAlignment = -4108 'Center in cells

        'Heading 4
        Dim rangeHeading4 As Object
        Dim endRangeHeading4 As String
        endCellNumber = HeadingCnt4
        endRangeHeading4 = ChrW(65 + endCellNumber) + "4"
        rangeHeading4 = worksheet1.Range("A4", endRangeHeading4)
        Dim heading4Array(HeadingCnt4) As String
        For ik = 0 To HeadingCnt4 - 1
            heading4Array(ik) = U2Array4.Extract(1, ik + 1).ToString
        Next
        rangeHeading4.Value2 = heading4Array
        'rangeHeading4.HorizontalAlignment = -4108 'Center in cells

        'Heading 5
        Dim rangeHeading5 As Object
        Dim endRangeHeading5 As String
        endCellNumber = HeadingCnt5
        endRangeHeading5 = ChrW(65 + endCellNumber) + "5"
        rangeHeading5 = worksheet1.Range("A5", endRangeHeading5)
        Dim heading5Array(HeadingCnt5) As String
        For ik = 0 To HeadingCnt5 - 1
            heading5Array(ik) = U2Array5.Extract(1, ik + 1).ToString
        Next
        rangeHeading5.Value2 = heading5Array
        'rangeHeading5.HorizontalAlignment = -4108 'Center in cells

        'Heading 6
        Dim rangeHeading6 As Object
        Dim endRangeHeading6 As String
        endCellNumber = HeadingCnt6
        endRangeHeading6 = ChrW(65 + endCellNumber) + "6"
        rangeHeading6 = worksheet1.Range("A6", endRangeHeading6)
        Dim heading6Array(HeadingCnt6) As String
        For ik = 0 To HeadingCnt6 - 1
            heading6Array(ik) = U2Array6.Extract(1, ik + 1).ToString
        Next
        rangeHeading6.Value2 = heading6Array
        'rangeHeading6.HorizontalAlignment = -4108 'Center in cells

        ' Column headings
        Dim rangeColHeading As Object
        Dim endRangeColHeading As String
        endCellNumber = ColHeadingCnt
        endRangeColHeading = ChrW(65 + endCellNumber) + "7"
        rangeColHeading = worksheet1.Range("A7", endRangeColHeading)
        Dim colHeadingArray(ColHeadingCnt) As String
        For ik = 0 To ColHeadingCnt - 1
            colHeadingArray(ik) = U2Array7.Extract(1, ik + 1).ToString
        Next
        rangeColHeading.Value2 = colHeadingArray
        'rangeColHeading.HorizontalAlignment = -4108 'Center in cells

        'Part number data
        Dim rangeParts As Object
        Dim endRangeParts As String

        linesCnt = U2Array8.Dcount(1) 'Number of lines of parts = number of values
        endCellNumber = linesCnt
        infoCnt = ColHeadingCnt ' Number of columns of information per line
        endRangeParts = ChrW(65 + infoCnt) + (linesCnt + 8).ToString
        rangeParts = worksheet1.Range("A8", endRangeParts.ToString)
        Dim partsArray(linesCnt, infoCnt) As String '(Column)
        Dim ig, ih As Integer
        For ig = 0 To linesCnt - 1
            For ih = 0 To infoCnt - 1
                partsArray(ig, ih) = U2Array8.Extract(1, ig + 1, ih + 1).ToString
            Next
        Next
        rangeParts.Value2 = partsArray
        'rangeParts.HorizontalAlignment = -4108 'Center in cells

    End Sub 'Setup_Spreadsheet()

    Private Sub AddDocumentLocatorFiles()

        Dim i As Long = 0
        Dim linesCnt As Integer = 0
        Dim sSQL As String = String.Empty
        Dim sUserID As String = String.Empty
        Dim sConString As String = String.Empty
        Dim sWorkingPath As String = String.Empty
        Dim sRepositoryName As String = String.Empty
        Dim sAssemblyDrawingPath As String = String.Empty
        Dim sInstructionPath As String = String.Empty
        Dim sPackagingPath As String = String.Empty
        Dim sSchematicPath As String = String.Empty
        Dim sAssemblyDrawingMSG As String = String.Empty
        Dim sInstructionMSG As String = String.Empty
        Dim sPackagingMSG As String = String.Empty
        Dim sSchematicMSG As String = String.Empty
        Dim sAssemblyDrawingVersionID As String = String.Empty
        Dim sInstructionVersionID As String = String.Empty
        Dim sPackagingVersionID As String = String.Empty
        Dim sSchematicVersionID As String = String.Empty
        Dim sAssemblyDrawingFileName As String = String.Empty
        Dim sAssemblyDrawingRevision As String = String.Empty
        Dim sSchematicFileName As String = String.Empty
        Dim sSchematicRevision As String = String.Empty
        Dim sLocation As String = String.Empty
        Dim sAppPath As String = String.Empty
        Dim sLogPath As String = String.Empty
        Dim sLogging As String = String.Empty
        Dim sResetLogging As String = String.Empty
        Dim sBOMSubFolder As String = String.Empty
        Dim sReturn As String = String.Empty
        Dim oCSSCon As Object
        Dim oConn As New System.Data.SqlClient.SqlConnection
        Dim oSQLCommand As New System.Data.SqlClient.SqlCommand
        Dim oReader As SqlClient.SqlDataReader
        Dim oGetVersion As Object
        Dim attribute As System.IO.FileAttributes = FileAttributes.Normal
        Dim bAssemblyDrawing As Boolean = False
        Dim bInstruction As Boolean = False
        Dim bPackaging As Boolean = False
        Dim bSchematics As Boolean = False
        Dim bStopLooping As Boolean = False
        Dim oColPaths As Collection

        'INITIATE OBJECTS
        oColPaths = New Collection
        oGetVersion = CreateObject("CSSDBLIB.GetVersion")

        'READ INI FILE
        sRepositoryName = modINI.ReadINI("STARTUP", "Repository", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))
        sLogPath = modINI.ReadINI("STARTUP", "LogPath", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))
        sLogging = modINI.ReadINI("STARTUP", "Logging", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))
        sBOMSubFolder = modINI.ReadINI("STARTUP", "BOMSubFolder", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))
        sResetLogging = modINI.ReadINI("STARTUP", "ResetLogging", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))

        ' CHECK IF THE OUTPUT LOG SHOULD BE RESET WITH EACH APPLICATION RUN
        If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
            If sResetLogging.ToLower() = "true" Then
                If File.Exists(sLogPath) = True Then
                    File.Delete(sLogPath)
                End If
            End If
        End If

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Entering Routine...")
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Repository: " & sRepositoryName)
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Passed in PartNumber / Revision: " & sPartNumber & " " & sRevision)
        End If

        ' GET CONNECTION STRING FROM THE CATALOG FILE
        oCSSCon = System.Activator.CreateInstance(System.Type.GetTypeFromProgID("CSSConnection.CSSCon"))
        With oCSSCon
            .Test = "0229EA40B8784839A5599770C7A23A"
            .Connection.CommandTimeout = 10
            .Connection.ConnectionTimeout = 10

            sConString = .LoadClientToken(sRepositoryName, sUserID, False)

            If Len(sConString) = 0 Then
                If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
                    LogEvent(sLogPath, "AddDocumentLocatorFiles | Could not retrieve connection string from CSSConnection.CSSCon")
                End If
                Exit Sub
            End If
        End With

        ' VERIFY DIRECTORY EXISTS
        sWorkingPath = txtBOMDir & "\" & sBOMSubFolder & "\"

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Working Directory is: " & sWorkingPath)
        End If

        If Not (System.IO.Directory.Exists(sWorkingPath)) Then
            System.IO.Directory.CreateDirectory(sWorkingPath)
        End If

        sLocation = Assembly.GetExecutingAssembly().Location
        sAppPath = Path.GetDirectoryName(sLocation)

        ' DELETE ANY EXISTING FILES IN THAT DIRECTORY
        Dim sArrFileList As String() = Directory.GetFiles(sWorkingPath, "*.*")
        For Each sFile As String In sArrFileList
            File.Delete(sFile)
        Next

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Starting Looping process for Assembly and Schematic documents.")
        End If

        ' LOOP THROUGH BOM LOOKING FOR THE ASSEMBLY DRAWING AND SCHEMATIC (-50, -12)
        linesCnt = U2Array8.Dcount(1)
        For i = 0 To linesCnt
            sReturn = U2Array8.Extract(1, i, 2).ToString

            If Len(sReturn) >= 3 Then
                If Right$(sReturn, 3) = "-50" Then
                    sAssemblyDrawingFileName = sReturn
                    sAssemblyDrawingRevision = U2Array8.Extract(1, i, 3).ToString
                End If
                If Right$(sReturn, 3) = "-12" Then
                    sSchematicFileName = sReturn
                    sSchematicRevision = U2Array8.Extract(1, i, 3).ToString
                End If
            End If
        Next

        ' CHECK IF VALUES HAVE BEEN READ FOR BOTH THE ASSEMBLY DRAWING AND SCHEMATIC. IF NOT, LOOP THROUGH LOOKING FOR -ASSY AND -SCH EXTENSION
        For i = 0 To linesCnt
            sReturn = U2Array8.Extract(1, i, 2).ToString

            If Len(sReturn) >= 3 Then
                If LCase(Right$(sReturn, 3)) = "-assy" Then
                    If Len(sAssemblyDrawingFileName) = 0 Then
                        sAssemblyDrawingFileName = sReturn
                        sAssemblyDrawingRevision = U2Array8.Extract(1, i, 3).ToString
                    End If
                End If
                If LCase(Right$(sReturn, 3)) = "-sch" Then
                    If Len(sSchematicFileName) = 0 Then
                        sSchematicFileName = sReturn
                        sSchematicRevision = U2Array8.Extract(1, i, 3).ToString
                    End If
                End If
            End If
        Next

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Found the following Assembly Drawing: " & sAssemblyDrawingFileName & " " & sAssemblyDrawingRevision)
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Found the following Schematic: " & sSchematicFileName & " " & sSchematicRevision)
        End If

        'Create  Procedure [dbo].[_HMEGetLabViewFiles] 
        '@PartNumber as nvarchar(50),
        '@AssemblyFileName as nvarchar(50),
        '@AssemblyRevision as nvarchar(50),
        '@SchematicFileName as nvarchar(50),
        '@SchematicRevision as nvarchar(50),
        '@UserID as nvarchar(50),
        '@WorkingPath as nvarchar(50)

        ' QUERY FOR THE DOCUMENT GUIDS
        sSQL = ""
        sSQL = sSQL & "Exec _HMEGetLabViewFiles '" & Replace(sPartNumber, "'", "''") & "', '" & Replace(sAssemblyDrawingFileName, "'", "''") & "', '" & Replace(sAssemblyDrawingRevision, "'", "''")
        sSQL = sSQL & "', '" & Replace(sSchematicFileName, "'", "''") & "', '" & Replace(sSchematicRevision, "'", "''") & "', '" & Replace(sUserID, "'", "''") & "', '" & Replace(sWorkingPath, "'", "''") & "' "

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Calling _HMEGetLabViewFiles stored procedure " & sSQL)
        End If

        oConn.ConnectionString = sConString.Substring(20) 'REMOVE PROVIDER INFORMATION
        oSQLCommand.Connection = oConn
        oConn.Open()

        oSQLCommand.CommandText = sSQL
        oReader = oSQLCommand.ExecuteReader()

        ' LOOP THROUGH TEMPLATE DOCUMENTS FOR APPROPRIATE GUIDS AND ADD TO THE GETLATEST OBJECT FILES COLLECTION
        If oReader.HasRows = True Then

            While oReader.Read()

                If oGetVersion.Connect(sRepositoryName) = True Then

                    ' SET GET LATEST PARAMETERS
                    oGetVersion.Files.Clear() 'CLEAR ANY EXISTING FILES IN THE LIST
                    oGetVersion.Errors.Clear()
                    oGetVersion.Launch = False 'OPTIONAL LAUNCH FLAG (TRUE OR FALSE)
                    oGetVersion.WorkingPath = sWorkingPath
                    'oGetVersion.WriteSessionFile = False
                    oGetVersion.DisableMessageQue = True

                    If Len(oReader.Item("ID")) > 0 Then
                        oGetVersion.Files.Add("Objects||" & oReader.Item("ID"), 0, CInt(oReader.Item("VersionID")), 0) 'CAN USE GUID INSTEAD OF PATH IF KNOWN
                    End If

                    If Len(oReader.Item("WorkingPath").ToString) > 0 Then
                        oColPaths.Add(oReader.Item("WorkingPath").ToString)
                    End If

                    If oReader.Item("FileType").ToString = "Instructions" Then
                        If Len(oReader.Item("WorkingPath").ToString) > 0 Then
                            sInstructionPath = oReader.Item("WorkingPath").ToString
                            bInstruction = True
                        End If
                        sInstructionMSG = oReader.Item("MessageText").ToString
                        sInstructionVersionID = oReader.Item("VersionID").ToString
                    End If

                    If oReader.Item("FileType").ToString = "Packaging" Then
                        If Len(oReader.Item("WorkingPath").ToString) > 0 Then
                            sPackagingPath = oReader.Item("WorkingPath").ToString
                            bPackaging = True
                        End If
                        sPackagingMSG = oReader.Item("MessageText").ToString
                        sPackagingVersionID = oReader.Item("VersionID").ToString
                    End If

                    If oReader.Item("FileType").ToString = "Assembly Drawing" Then
                        If Len(oReader.Item("WorkingPath").ToString) > 0 Then
                            sAssemblyDrawingPath = oReader.Item("WorkingPath").ToString
                            bAssemblyDrawing = True
                        End If
                        sAssemblyDrawingMSG = oReader.Item("MessageText").ToString
                        sAssemblyDrawingVersionID = oReader.Item("VersionID").ToString
                    End If

                    If oReader.Item("FileType").ToString = "Schematics" Then
                        If Len(oReader.Item("WorkingPath").ToString) > 0 Then
                            sSchematicPath = oReader.Item("WorkingPath").ToString
                            bSchematics = True
                        End If
                        sSchematicMSG = oReader.Item("MessageText").ToString
                        sSchematicVersionID = oReader.Item("VersionID").ToString
                    End If

                    ' EXECUTE GET LATEST METHOD
                    If oGetVersion.Files.Count > 0 Then
                        oGetVersion.Execute(ShowProgress:=False, ShowDialogs:=False)

                        If oGetVersion.Errors.Count = 0 Then
                            'SUCCESS
                        Else
                            If oGetVersion.Errors.Count > 0 Then
                                For i = 1 To oGetVersion.Errors.Count
                                    If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
                                        LogEvent(sLogPath, "AddDocumentLocatorFiles | GetVersion Error: " & oGetVersion.Errors.Item(i).ErrDescription)
                                    End If
                                Next i
                            End If
                        End If
                    Else
                        If sLogging.ToLower() = "advanced" Then
                            LogEvent(sLogPath, "AddDocumentLocatorFiles | No files to download from GetVersion for: " & oReader.Item("FileType").ToString)
                        End If
                    End If

                Else
                    If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
                        LogEvent(sLogPath, "AddDocumentLocatorFiles | .Connect method returned false from GetVersion")
                    End If
                End If

            End While

            Try
                ' REMOVE READONLY FLAGS
                For i = 1 To oColPaths.Count
                    File.SetAttributes(oColPaths(i), attribute)
                Next

                Dim sArrFileList2 As String() = Directory.GetFiles(sWorkingPath, "*.ses")
                For Each sFile As String In sArrFileList2
                    File.Delete(sFile)
                Next
            Catch ex As Exception
                If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
                    LogEvent(sLogPath, "AddDocumentLocatorFiles | Try/Catch Error message: " & ex.Message.ToString())
                End If
            End Try

            ' CLOSE OBJECT ON SUCCESSFUL EXPORT
            oReader.Close()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oGetVersion)
            oGetVersion = Nothing

        Else
            If sLogging.ToLower() = "true" Or sLogging.ToLower() = "advanced" Then
                LogEvent(sLogPath, "AddDocumentLocatorFiles | No rows were returned from _HMEGetLabViewFiles")
            End If
        End If

        ' SZ - 2014-01-29 - UPDATED TO NOT SEND THE BLANK FILE TO THE OUTPUT FOLDER PER RANDY'S REQUEST
        'SAVE OFF PATH INFORMATION OF EITHER A BLANK FILE OR THE CORRECT FILE PROVIDED
        'If bInstruction = False Then
        '    sInstructionPath = sWorkingPath & sPartNumber & "-57.pdf"
        '    File.Copy(sAppPath & "\blank.pdf", sInstructionPath)
        'End If

        'If bPackaging = False Then
        '    sPackagingPath = sWorkingPath & sPartNumber & "-62.pdf"
        '    File.Copy(sAppPath & "\blank.pdf", sPackagingPath)
        'End If

        'If bAssemblyDrawing = False Then
        '    sAssemblyDrawingPath = sWorkingPath & sPartNumber & "-50.pdf"
        '    File.Copy(sAppPath & "\blank.pdf", sAssemblyDrawingPath)
        'End If

        'If bSchematics = False Then
        '    sSchematicPath = sWorkingPath & sPartNumber & "-12.pdf"
        '    File.Copy(sAppPath & "\blank.pdf", sSchematicPath)
        'End If

        ' WRITE TO DL ARRAY
        sArray(0) = "Assembly Drawing:"
        sArray(1) = sAssemblyDrawingPath
        sArray(2) = sAssemblyDrawingMSG
        sArray(3) = sAssemblyDrawingVersionID
        sArray(4) = "Instructions:"
        sArray(5) = sInstructionPath
        sArray(6) = sInstructionMSG
        sArray(7) = sInstructionVersionID
        sArray(8) = "Packaging:"
        sArray(9) = sPackagingPath
        sArray(10) = sPackagingMSG
        sArray(11) = sPackagingVersionID
        sArray(12) = "Schematics:"
        sArray(13) = sSchematicPath
        sArray(14) = sSchematicMSG
        sArray(15) = sSchematicVersionID

        If sLogging.ToLower() = "advanced" Then
            LogEvent(sLogPath, "AddDocumentLocatorFiles | Exiting Routine")
        End If

    End Sub

    Private Sub WorksheetSaveAs()

        'Save the file - but first check for the version of Excel used
        ' xlExcel8 is the 97-2003 file compatible format. xlExcel8 doesn't exist 
        'in Excel Versions < 12.0
        Try
            SourceDirExists() 'Check to see if Directory exists - create it if it doesn't
            SourceFileExists() 'Check to see if file exists - delete it if it does
            'MsgBox(objExcelApp.Version) ' Display Excel version number
            Dim objFileFormat1 As Object
            If Val(objExcelApp.Version) < 12 Then
                workbook.SaveCopyAs(txtBOMDir & "\" & txtBOMFile)
                workbook.Close(False) 'Don't prompt to save workbook
            Else
                objFileFormat1 = 56 ' 56 = xlExcel8 in Excel 12.0
                workbook.SaveAs(txtBOMDir & "\" & txtBOMFile, objFileFormat1)
                workbook.Close(False) 'Don't prompt to save workbook
            End If
        Catch ex As Exception
            MsgBox(ex.Message + "  Error 25")
            Exit Sub
        End Try
    End Sub

    Private Sub SourceFileExists()
        If System.IO.File.Exists(txtBOMDir + "\" + txtBOMFile) Then 'File exists - delete it
            System.IO.File.Delete(txtBOMDir + "\" + txtBOMFile)
        End If
    End Sub

    Private Sub SourceDirExists()
        If Not (System.IO.Directory.Exists(txtBOMDir)) Then 'Directory doesn't exist - create it
            System.IO.Directory.CreateDirectory(txtBOMDir)
        End If
    End Sub

    Private Function LogEvent(ByVal pLogPath As String, ByVal pMessage As String) As Boolean


        Dim fs1 As FileStream = New FileStream(pLogPath, FileMode.Append, FileAccess.Write)
        Dim s1 As StreamWriter = New StreamWriter(fs1)

        s1.Write(Now() & " " & pMessage & vbCrLf)

        s1.Close()
        fs1.Close()
        LogEvent = True


    End Function

    Private Function GetBom(ByVal itemId As String, ByRef ds As DataSet)

        Dim connectionString As String
        connectionString = modINI.ReadINI("STARTUP", "ConnectionString", System.Reflection.Assembly.GetExecutingAssembly().Location.Replace(".dll", ".ini"))

        Dim conn As New SqlConnection
        Dim dataSet As New DataSet()
        If conn.State = ConnectionState.Closed Then
            conn.ConnectionString = connectionString

        End If

        Try
            conn.Open()

            Dim sqlquery As String = "
                SELECT ItemId, COUNT(ItemId) count
                FROM BOMVersion
                WHERE  ItemId = @itemId
                GROUP BY ItemId
                HAVING ( COUNT(ItemId) > 1 );

                select A.ItemId, B.ECMRevLevelDrawing, B.NameAlias, C.UnitId, A.BOMId, A.Active, A.Approved, A.FromDate, A.ToDate, A.FromQty, A.DataAreaId 
                from BOMVersion as A
                inner join InventTable as B on A.ItemId = B.ItemId
                inner join InventTableModule as C on B.ItemId = C.ItemId
                where C.ModuleType = 0 
                and C.DataAreaId = 'hsc' 
                and  B.DataAreaId = 'hsc' 
                and A.Active = 1 and A.Approved = 1 
                and A.DataAreaId = 'hsc' 
                and A.ItemId = @itemId;

               select A.LineNum, A.ItemId, B.ECMRevLevelDrawing,
                '' AS AM_RoHSCompliant,--B.AM_RoHSCompliant,
                B.NameAlias, A.UnitId, A.BomQty,
                (select stuff((select  ',' + C.RefDesignator from SemBOMRefDesignatorTable as C where C.DataAreaId = 'hsc' AND C.BOM = A.RecId and C.RefDesignator != '' group by C.RefDesignator for xml path('')), 1, 1, '')) as RefDesignator,
                (select sum(D.PhysicalInvent) from InventSumAggrDeltaView as D where D.DataAreaId = 'hsc' AND D.ItemId = A.ItemId group by D.ItemId) as PhysicalInvent,
                (select stuff((select  ',' + D.ItemManPartNo from SemInventItemManufacturers as D where D.DataAreaId = 'hsc' AND  D.ItemId = A.ItemId group by D.ItemManPartNo for xml path('')), 1, 1, '')) as ManPartNo
                from BOM as A
                inner join InventTable as B on A.ItemId = B.ItemId
                        where B.DataAreaId = 'hsc' and
                                A.DataAreaId = 'hsc' and A.BOMId =  @itemId order by A.LineNum asc"

            Dim adapter As New SqlDataAdapter
            Dim parameter As New SqlParameter
            Dim command As SqlCommand = New SqlCommand(sqlquery, conn)
            With command.Parameters
                .Add(New SqlParameter("itemId", itemId))
            End With
            command.Connection = conn
            Dim SqlDataAdapter As New SqlDataAdapter(command)
            SqlDataAdapter.Fill(dataSet)

        Catch ex As Exception
            MsgBox("error" & vbCrLf & ex.Message)
        End Try
        ds = dataSet
        Return dataSet
    End Function
End Class
