﻿
Public Class cINI

    Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" _
        (ByVal lpSectionName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Long

    Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" _
        (ByVal lpSectionName As String, ByVal lpKeyName As String, ByVal lpValue As String, ByVal lpFilename As String) As Long


    Public sPath As String = String.Empty

    Public Sub INIFile(ByVal INIPath As String)

        sPath = INIPath
        sPath = sPath.Replace("file:\\", "")

    End Sub

    Public Sub IniWriteValue(ByVal Section As String, ByVal Key As String, ByVal Value As String)

        WritePrivateProfileString(Section, Key, Value, sPath)

    End Sub

    Public Function IniReadValue(ByVal Section As String, ByVal Key As String) As String

        Dim lOutput As Long = 0
        Dim sOutput As String = String.Empty

        lOutput = GetPrivateProfileString(Section, Key, "", sOutput, 255, sPath)
        IniReadValue = sOutput.ToString()

    End Function

End Class
