﻿
Module BOMLoader

    Sub Main(ByVal args() As String)

        Dim PartNumber As String = args(0)
        Dim RevLevel As String = args(1)
        Dim oBillOfMaterials As BillOfMaterials.cAction

        oBillOfMaterials = New BillOfMaterials.cAction

        With oBillOfMaterials
            .DownloadBOM(PartNumber, RevLevel)
        End With

        If Not oBillOfMaterials Is Nothing Then oBillOfMaterials = Nothing

    End Sub

End Module

